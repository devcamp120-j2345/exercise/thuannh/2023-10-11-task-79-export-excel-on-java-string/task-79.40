package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerbarchart.model.CarType;

public interface ICarTypeRepository extends JpaRepository<CarType, Long> {

}
