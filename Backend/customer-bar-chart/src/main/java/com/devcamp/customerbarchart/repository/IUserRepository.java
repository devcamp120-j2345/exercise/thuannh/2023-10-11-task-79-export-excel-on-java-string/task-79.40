package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.customerbarchart.model.CUser;

@Repository
public interface IUserRepository extends JpaRepository<CUser, Long> {
	CUser findByUsername(String username);
	// CUser findByEmail(String email);

}
