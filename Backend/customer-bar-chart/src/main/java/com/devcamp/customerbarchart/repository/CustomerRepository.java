package com.devcamp.customerbarchart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.customerbarchart.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "SELECT country, COUNT(*) AS customer_count FROM customers WHERE country IN ('USA', 'France', 'Singapore', 'Spain') GROUP BY country", nativeQuery = true)
    List<Object[]> getCustomerCountsByCountry();
}
